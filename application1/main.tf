provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "ccoe-ir-map-tooling"
    key    = "ccoe-ir-map-tooling/tf_state"
    region = "eu-west-1"
  }
}

module "s3"{
  source = "../Moudules/aws_s3"
  bucketname = ""
  }

/*module "networking" {
  source         = "../Modules/aws_networking"
  vpctag         = "vpc1"
  vpc_cidr       = "11.0.0.0/16"
  PubsSbnet_cidr = ["11.0.1.0/24", "11.0.2.0/24", "11.0.3.0/24"]
}

module "sg" {
  source           = "../Modules/aws_sg"
  sg-name          = "sg1"
  sg-description   = "Sg1 for Testing"
  vpc_id           = "${module.networking.vpc_id}"
  ingrees_fromport = "22"
  ingres_Toport    = "22"
  protocol         = "TCP"
}*/

/*module "eni" {
  source = "../Modules/aws_eni"
  subnet_id = ""
  eni_name = "eni1"
  private_ips = ["11.0.1.0/24" , "11.0.2.0/24", "11.0.3.0/24"]
  sg_id = "${module.sg.sg_id}"
  ec2_id = "${module.ec2.ec2.instance_id}"
}


module "ec2" {
  source = "../Modules/aws_ec2"
  key = "terraform"
  ec2 = "t2.micro"
  ec2_tag = ""
}*/

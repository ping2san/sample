variable "region" {
  default = "eu-west-1"
}

variable "s3bucket" {
  default = "ccoe-ir-map-tooling"
}

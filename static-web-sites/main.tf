provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "ccoe-ir-map-tooling"
    key    = "ccoe-ir-map-tooling/tf_state"
    region = "eu-west-1"
  }
}

module "ec2" {
  source  = "../Modules/aws_ec2"
  key     = "terraform"
  ec2     = "t2.micro"
  ec2_tag = "ccoe-dev-web"
}

module "s3" {
  source      = "../Modules/aws_s3"
  bucketname  = "ccoe-map-app-dev"
  bucketname1 = "ccoe-map-app-prod"
}

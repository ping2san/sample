data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.ec2}"
  key_name      = "${var.key}"
  #count = "length(var.subnet_ids)"

  tags = {
    Name = "${var.ec2_tag}"
  }
}

output "instance_id" {
  value = "${aws_instance.web.id}"
}

resource "aws_network_interface" "code_eni" {
  description = "${var.eni_name}"
  subnet_id       = "${var.subnet_id.}"
  private_ips     = ["${var.private_ips}"]
  security_groups = ["${var.sg_id}"]

  attachment {
    instance     = "${var.ec2_id}"
    device_index = 1
  }

 }
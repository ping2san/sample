variable "subnet_id" {
  description = "Define the Subnet ID "
}
variable "eni_name" {
  description = "Input the ENI Name on the main Module"
  }

variable "private_ips" {
  type = list 
  description = "Input the private IPs"
}

variable "sg_id" {
  type = list
  description = "Security Group Id to be placed "  
}

variable "ec2_id" {}

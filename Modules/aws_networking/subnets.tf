resource "aws_subnet" "pubsubnet" {
  count = "${length(data.aws_availability_zones.azs.names)}"
  availability_zone = "${element(data.aws_availability_zones.azs.names, count.index)}"
  vpc_id     = "${aws_vpc.code_vpc.id}"
  cidr_block = "${element(var.PubsSbnet_cidr,count.index)}"
   
tags = {
    Name = "pubsubnet-${count.index+1}"
  }
}
